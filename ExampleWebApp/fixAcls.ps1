$StartingDir = 'C:\inetpub\wwwroot\ExampleWebApp_deploy';

Write-Host 'Fixing ACL for:C:\inetpub\wwwroot\ExampleWebApp_deploy';
$path = 'C:\inetpub\wwwroot\ExampleWebApp_deploy';
$aclfix = Get-Acl $path;
Set-Acl $path $aclfix;
$rule = New-Object System.Security.AccessControl.FileSystemAccessRule('NetworkService', 'FullControl', 'Allow');
foreach ($file in $(Get-ChildItem $StartingDir -recurse | ?{ $_.PSIsContainer })) {
  Write-Host 'Setting ACL for: '$file.FullName;
  $Acl=get-acl $file.FullName;
  #Fix connonical form in ACL
  $Acl.SetAccessRule($rule);
  set-acl $File.Fullname $Acl;
};